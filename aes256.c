#include <inttypes.h>
#include <stdio.h>

/* Implement Arenas */
#include <stdlib.h>

typedef uint32_t u32;
typedef uint8_t u8;

/* This are specific constants for AES 256 */
#define NB 4
#define NK 8
#define NR 14

/* I've read somewhere this is a bad practic. Get Good */
typedef u8 State[4][NB];

static const u8 sbox[16][16] = {
	/*        0     1    2      3     4    5     6     7      8    9     A      B    C     D     E     F    */
	/* 0 */{ 0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76 },
	/* 1 */{ 0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0 },
	/* 2 */{ 0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15 },
	/* 3 */{ 0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75 },
	/* 4 */{ 0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84 },
	/* 5 */{ 0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf },
	/* 6 */{ 0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8 },
	/* 7 */{ 0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2 },
	/* 8 */{ 0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73 },
	/* 9 */{ 0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb },
	/* A */{ 0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79 },
	/* B */{ 0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08 },
	/* C */{ 0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a },
	/* D */{ 0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e },
	/* E */{ 0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf },
	/* F */{ 0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16 }
};

static const u8 inv_sbox[16][16] = {
	{0x52, 0x09, 0x6a, 0xd5, 0x30, 0x36, 0xa5, 0x38, 0xbf, 0x40, 0xa3, 0x9e, 0x81, 0xf3, 0xd7, 0xfb},
	{0x7c, 0xe3, 0x39, 0x82, 0x9b, 0x2f, 0xff, 0x87, 0x34, 0x8e, 0x43, 0x44, 0xc4, 0xde, 0xe9, 0xcb},
	{0x54, 0x7b, 0x94, 0x32, 0xa6, 0xc2, 0x23, 0x3d, 0xee, 0x4c, 0x95, 0x0b, 0x42, 0xfa, 0xc3, 0x4e},
	{0x08, 0x2e, 0xa1, 0x66, 0x28, 0xd9, 0x24, 0xb2, 0x76, 0x5b, 0xa2, 0x49, 0x6d, 0x8b, 0xd1, 0x25},
	{0x72, 0xf8, 0xf6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xd4, 0xa4, 0x5c, 0xcc, 0x5d, 0x65, 0xb6, 0x92},
	{0x6c, 0x70, 0x48, 0x50, 0xfd, 0xed, 0xb9, 0xda, 0x5e, 0x15, 0x46, 0x57, 0xa7, 0x8d, 0x9d, 0x84},
	{0x90, 0xd8, 0xab, 0x00, 0x8c, 0xbc, 0xd3, 0x0a, 0xf7, 0xe4, 0x58, 0x05, 0xb8, 0xb3, 0x45, 0x06},
	{0xd0, 0x2c, 0x1e, 0x8f, 0xca, 0x3f, 0x0f, 0x02, 0xc1, 0xaf, 0xbd, 0x03, 0x01, 0x13, 0x8a, 0x6b},
	{0x3a, 0x91, 0x11, 0x41, 0x4f, 0x67, 0xdc, 0xea, 0x97, 0xf2, 0xcf, 0xce, 0xf0, 0xb4, 0xe6, 0x73},
	{0x96, 0xac, 0x74, 0x22, 0xe7, 0xad, 0x35, 0x85, 0xe2, 0xf9, 0x37, 0xe8, 0x1c, 0x75, 0xdf, 0x6e},
	{0x47, 0xf1, 0x1a, 0x71, 0x1d, 0x29, 0xc5, 0x89, 0x6f, 0xb7, 0x62, 0x0e, 0xaa, 0x18, 0xbe, 0x1b},
	{0xfc, 0x56, 0x3e, 0x4b, 0xc6, 0xd2, 0x79, 0x20, 0x9a, 0xdb, 0xc0, 0xfe, 0x78, 0xcd, 0x5a, 0xf4},
	{0x1f, 0xdd, 0xa8, 0x33, 0x88, 0x07, 0xc7, 0x31, 0xb1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xec, 0x5f},
	{0x60, 0x51, 0x7f, 0xa9, 0x19, 0xb5, 0x4a, 0x0d, 0x2d, 0xe5, 0x7a, 0x9f, 0x93, 0xc9, 0x9c, 0xef},
	{0xa0, 0xe0, 0x3b, 0x4d, 0xae, 0x2a, 0xf5, 0xb0, 0xc8, 0xeb, 0xbb, 0x3c, 0x83, 0x53, 0x99, 0x61},
	{0x17, 0x2b, 0x04, 0x7e, 0xba, 0x77, 0xd6, 0x26, 0xe1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0c, 0x7d}
};

/* Only storing 11 powers in NR changes we will need more */
static const u8 rcon[11] = {0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36 };

#define SBOX(a) sbox[((a) & 0xF0) >> 4][(a) & 0x0F]
#define INV_SBOX(a) inv_sbox[((a) & 0xF0) >> 4][(a) & 0xF0]

void
sub_bytes(State s)
{
	/* This is implemented thorugh a lookup table, not sure if it is faster
	 * than the bit level computation. */
	for (int c = 0; c < NB; c++) {
		for (int i = 0; i < 4; i++) {
			s[i][c] = SBOX(s[i][c]);
		}
	}
}

void
shift_rows(State s)
{
	u8 tmp = s[1][0];
	s[1][0] = s[1][1];
	s[1][1] = s[1][2];
	s[1][2] = s[1][3];
	s[1][3] = tmp;

	tmp = s[2][0];
	s[2][0] = s[2][2];
	s[2][2] = tmp;
	tmp = s[2][1];
	s[2][1] = s[2][3];
	s[2][3] = tmp;

	tmp = s[3][3];
	s[3][3] = s[3][2];
	s[3][2] = s[3][1];
	s[3][1] = s[3][0];
	s[3][0] = tmp;
}

u8
times_02(u8 in)
{
	int b7 = in & 0x80;
	u8 r = in << 1;
	if (b7)
		r ^= 0x1b;
	return r;
}

/* TODO: This is slow as heck */
u8
times(u8 a, u8 b)
{
	u8 r = 0;
	for (int i = 0; i < 8; i++) {
		if ((b >> i) & 0x1)
			r = r ^ a;
		a = times_02(a);
	}
	return r;
}

void
mix_columns(State s)
{
	for (int c = 0; c < NB; c++) {
		u8 s0 = s[0][c], s1 = s[1][c], s2 = s[2][c], s3 = s[3][c];
		s[0][c] = times(s0, 0x02) ^ times(s1, 0x03) ^ s2 ^ s3;
		s[1][c] = s0 ^ times(s1, 0x02) ^ times(s2, 0x03) ^ s3;
		s[2][c] = s0 ^ s1 ^ times(s2, 0x02) ^ times(s3, 0x03);
		s[3][c] = times(s0, 0x03) ^ s1 ^ s2 ^ times(s3, 0x02);
	}
}

void
add_round_key(State s, u8 *w)
{
	for (int c = 0; c < NB; c++) {
		for (int i = 0; i < 4; i++) {
			s[i][c] = s[i][c] ^ w[4*c + i];
		}
	}
}

/* u8[NB*(NR+1)*4] */
u8 *
key_expansion(u8 key[4*NK])
{
	u8 *w = malloc(NB*(NR+1)*4);
	for (int i = 0; i < NK; i++) {
		w[4*i+0] = key[4*i+0];
		w[4*i+1] = key[4*i+1];
		w[4*i+2] = key[4*i+2];
		w[4*i+3] = key[4*i+3];
	}

	for (int i = NK; i < NB*(NR+1); i++) {
		u8 t0 = w[4*(i-1)+0];
		u8 t1 = w[4*(i-1)+1];
		u8 t2 = w[4*(i-1)+2];
		u8 t3 = w[4*(i-1)+3];

		if (i % NK == 0) {
			u8 tmp = t0;
			t0 = SBOX(t1) ^ rcon[i/NK];
			t1 = SBOX(t2);
			t2 = SBOX(t3);
			t3 = SBOX(tmp);
		} else if (NK > 6 && i % NK == 4) {
			t0 = SBOX(t0);
			t1 = SBOX(t1);
			t2 = SBOX(t2);
			t3 = SBOX(t3);
		}

		w[4*i+0] = w[4*(i-NK)+0] ^ t0;
		w[4*i+1] = w[4*(i-NK)+1] ^ t1;
		w[4*i+2] = w[4*(i-NK)+2] ^ t2;
		w[4*i+3] = w[4*(i-NK)+3] ^ t3;
	}
	return w;
}

void
inv_shift_rows(State s)
{
	u8 tmp = s[1][3];
	s[1][3] = s[1][2];
	s[1][2] = s[1][1];
	s[1][1] = s[1][0];
	s[1][0] = tmp;

	tmp = s[2][0];
	s[2][0] = s[2][2];
	s[2][2] = tmp;
	tmp = s[2][1];
	s[2][1] = s[2][3];
	s[2][3] = tmp;

	tmp = s[3][0];
	s[3][0] = s[3][1];
	s[3][1] = s[3][2];
	s[3][2] = s[3][3];
	s[3][3] = tmp;
}

void
inv_sub_bytes(State s)
{
	/* This is implemented thorugh a lookup table, not sure if it is faster
	 * than the bit level computation. */
	for (int c = 0; c < NB; c++) {
		for (int i = 0; i < 4; i++) {
			s[i][c] = INV_SBOX(s[i][c]);
		}
	}
}

void
inv_mix_columns(State s)
{
	for (int c = 0; c < NB; c++) {
		u8 s0 = s[0][c], s1 = s[1][c], s2 = s[2][c], s3 = s[3][c];
		s[0][c] = times(s0, 0x0e) ^ times(s1, 0x0b) ^ times(s2, 0x0d) ^ times(s3, 0x09);
		s[1][c] = times(s0, 0x09) ^ times(s1, 0x0e) ^ times(s2, 0x0b) ^ times(s3, 0x0d);
		s[2][c] = times(s0, 0x0d) ^ times(s1, 0x09) ^ times(s2, 0x0e) ^ times(s3, 0x0b);
		s[3][c] = times(s0, 0x0b) ^ times(s1, 0x0d) ^ times(s2, 0x09) ^ times(s3, 0x0e);
	}
}

u8 *
encrypt(u8 in[NB*4], u8 key[4*NK])
{
	State s;
	u8 *w = key_expansion(key);

	for (int c = 0; c < NB; c++) {
		for (int i = 0; i < 4; i++) {
			s[i][c] = in[4*c + i];
		}
	}

	add_round_key(s, w);

	for (int round = 1; round < NR; round++) {
		sub_bytes(s);
		shift_rows(s);
		mix_columns(s);
		add_round_key(s, w + round*NB*4);
	}

	sub_bytes(s);
	shift_rows(s);
	add_round_key(s, w + NR*NB*4);

	u8 *out = malloc(NB*4);

	for (int c = 0; c < NB; c++) {
		for (int i = 0; i < 4; i++) {
			out[4*c + i] = s[i][c];
		}
	}

	return out;
}

u8 *
decrypt(u8 in[NB*4], u8 key[NK*4])
{
	State s;
	u8 *w = key_expansion(key);

	for (int c = 0; c < NB; c++) {
		for (int i = 0; i < 4; i++) {
			s[i][c] = in[4*c + i];
		}
	}

	add_round_key(s, w + NR*NB*4);

	for (int round = NR-1; round >= 1; round--) {
		inv_shift_rows(s);
		inv_sub_bytes(s);
		add_round_key(s, w + round*NB*4);
		inv_mix_columns(s);
	}

	inv_shift_rows(s);
	inv_sub_bytes(s);
	add_round_key(s, w);

	u8 *out = malloc(NB*4);
	for (int c = 0; c < NB; c++) {
		for (int i = 0; i < 4; i++) {
			out[4*c + i] = s[i][c];
		}
	}
	return out;
}
int
main()
{
	u8 input[] = {0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff};
	u8 key[] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f};

	u8 *result = encrypt(input, key);
	for (int i = 0; i < 16; i++) {
		printf("%02x ", result[i] & 0xff);
	}
	printf("\n");
	u8 *t = decrypt(result, key);
	for (int i = 0; i < 16; i++) {
		printf("%02x ", t[i] & 0xff);
	}
	printf("\n");


	return 0;
}
